﻿<#

  cryptofeels & Get-Price: Arfonzo's Cryptocurrency Price Checker

  - Uses cointmarketcap.com API.
  - Author: arfonzo, at gmail dot com.


INSTALLATION

- Put Get-Price.psm1 somewhere.
- Start PowerShell. 
- Edit your PS profile, for example: notepad $profile
- Add the following line, and close your $profile.

Import-Module C:\Some\Path\To\PS_btcbag\Get-Price.psm1;

- Open a new PowerShell session.
- You now have the following cmdlets:
    Get-Price
    cryptofeels


USAGE

  Get-Price -Help

  cryptofeels -Help


BASIC EXAMPLES

- List top 50 coins by percentage changed in the last 24h:
    cryptofeels 50 -Change24h

- Show detail for EGC, EverGreenCoin:
    Get-Price -Coin EGC

#>
function Get-Price {

    param (
        [string]$Currency = "BTC",
        [string]$Fiat = "GBP",
        [switch]$Raw,
        [switch]$ListCurrencies,
        [int]$Limit,
        [switch]$SortAlphabetical,
        [switch]$Sort1hChange,
        [switch]$Sort24hChange,
        [switch]$Sort7dChange,
        [switch]$Help
    )

    $fiat_list = @("AUD", "BRL", "CAD", "CHF", "CNY", "EUR", "GBP", "HKD", "IDR", "INR", "JPY", "KRW", "MXN", "RUB");

    if ($Help) {
        Write-Host "`n`n Arf's Cryptocoin Price Checker `n" -BackgroundColor DarkGreen -ForegroundColor Green

        Write-Host "`t-Help`t`t`t`tThis help page."
        Write-Host "`t-Currency <string>`tCryptocurrency symbol to query. Use the 'symbol' from the output of -ListCurrencies. Default: `"BTC`"."
        Write-Host "`t-Fiat <string>`t`tFiat currency to convert to. Default: `"$Fiat`". Valid values: AUD, BRL, CAD, CHF, CNY, EUR, GBP, HKD, IDR, INR, JPY, KRW, MXN, RUB."
        Write-Host "`t-Raw`t`t`tReturn only raw price value."
        Write-Host "`t-ListCurrencies`t`tList all supported cryptocurrencies, by market capital. Default: `$False."
        Write-Host "`t-Limit <int>`t`tLimit -ListCurrencies to <int> results. Default: no limit."
        Write-Host "`t-SortAlphabetical`tSort -ListCurrencies alphabetically by symbol."

        return $null;
    }

    # Check Fiat.
    if ($Fiat) {
        if ($fiat_list.IndexOf($Fiat.ToUpper()) -ne -1) {
        } else {
            Write-Error "Fiat currency not supported. Check -Help for supported currencies.";
            return;
        }
    }

    if ($ListCurrencies) {
        if ($Limit) {
            $status = Invoke-RestMethod -Method Get -Uri "https://api.coinmarketcap.com/v1/ticker?convert=$Fiat&limit=$Limit"
        } else {
            $status = Invoke-RestMethod -Method Get -Uri "https://api.coinmarketcap.com/v1/ticker/?convert=$Fiat"
        }

        # Cast as required
        $status | % { $_.rank = [int]$_.rank };
        $status | % { $_.percent_change_1h = [float]$_.percent_change_1h };
        $status | % { $_.percent_change_24h = [float]$_.percent_change_24h };
        $status | % { $_.percent_change_7d = [float]$_.percent_change_7d };

        if ($SortAlphabetical) {
            $status = $status | Sort-Object -Property symbol
        } elseif ($Sort1hChange) {
            $status = $status | Sort-Object -Property percent_change_1h -Descending
        } elseif ($Sort24hChange) {
            $status = $status | Sort-Object -Property percent_change_24h -Descending
        } elseif ($Sort7dChange) {
            $status = $status | Sort-Object -Property percent_change_7d -Descending
        }

        if ($Raw) {
            return $status;
        } else {
            $status|ft symbol,name,id,*change*
            return;
        }
    }

    # Match symbols with id.
    if ($Currency) {
        $cryptos = Invoke-RestMethod -Method Get -Uri "https://api.coinmarketcap.com/v1/ticker/"
        $crypto_found = $cryptos|Where-Object -Property symbol -EQ $Currency;
        
        if ($crypto_found) {
            #$crypto_found.id;
            $Currency = $crypto_found.id;
        } else {
            Write-Error "Crypto currency not found: $Currency.";
            return;
        }
    }

    $status = Invoke-RestMethod -Method Get -Uri "https://api.coinmarketcap.com/v1/ticker/$Currency/?convert=$($Fiat.ToUpper())"

    if ($status -ne $null) {
        if ($Raw) {
            if ($Fiat -eq "EUR") {
                return [float]($status.price_eur);
            } elseif ($Fiat -eq "GBP") {
                return [float]($status.price_gbp);
            } else {
                return [float]($status.price_usd);
            }
        } else {
            return $status;
        }
    }
  
}


# cryptofeels - current state of the crypto world
#   Requires module: Get-Price
$global:cryptos  = Import-Clixml -Path "$home\.cryptos.state"
$global:cryptofolio = @("BTC",
    "BCH",
    "DOGE",
    "EGC",
    "ETH",
    "ICN",
    "XLM",
    "XRP"
    );

function cryptofeels ([string]$Coin, [int]$Limit, [switch]$Silent, [switch]$Change1h, [switch]$Change24h, [switch]$Change7d, [switch]$Worst, [switch]$Help, [switch]$Portfolio) {

    if ($Help) {
        Write-Host "`n cryptofeels    HALP! `n" -BackgroundColor DarkCyan -ForegroundColor Green;

        Write-Host "`t-Help`t`tThis help."
        Write-Host "`t-Limit <int>`tLimit to <int> results."
        Write-Host "`t-Change1h`tSort by: 1 hour percentage change."
        Write-Host "`t-Change24h`tSort by: 24 hour percentage change."
        Write-Host "`t-Change7d`tSort by: 7 day percentage change."
        Write-Host "`t-Worst`t`tShow worst performers."
        Write-Host "`t-Silent`t`tRefresh `$global:cryptos object data with no output."
        Write-Host ""
        Write-Host "`t`$cryptos`t`tA global PowerShell object that contains market data. Use and manipulate it to explore market data. Running cryptofeels will update this object."
        Write-Host ""
        return;
    }

	$global:cryptos = Get-Price -ListCurrencies -Raw
	$global:cryptos|Export-Clixml -Path "$home\.cryptos.state"

    # Exclude all coins in Array, use 'name' property.
    $exclude = @("iCoin"
        );

	if ($Silent) {
		return;
	} else {
        Write-Host "`n cryptofeels    $(Get-Date)    $($PSBoundParameters.Keys) " -BackgroundColor DarkCyan -ForegroundColor Green;

        if ($exclude.Count -gt 0) {
            $exclude.ForEach({
                $cryptos = $cryptos|where name -NE $_;
                });
        }

        if ($Portfolio) {
            # Only display cryptos in portfolio.
            $cryptos = $cryptos|where ({$_.symbol -in $global:cryptofolio});
        }

        if ($Coin) {
            $cryptos|Where-Object symbol -EQ $Coin;
            return;
        }        

		if ($Limit) {
			if ($Change1h) {
				if ($Worst) {
					$c = $cryptos|Sort-Object -Property percent_change_1h
				} else {
					$c = $cryptos|Sort-Object -Property percent_change_1h -Descending
				}
				$c[0..($Limit-1)]|ft name, symbol,rank, price_btc, @{Label="price_gbp"; Expression={"£{0:N2}" -f [double]$_.price_gbp}}, @{Label="24h_volume_gbp"; Expression={"£{0:N2}" -f [double]$_.'24h_volume_gbp'}}, @{Label="market_cap_gbp"; Expression={"£{0:N0}" -f [double]$_.market_cap_gbp}}, *change*
			} elseif ($Change24h) {
				if ($Worst) {
					$c = $cryptos|Sort-Object -Property percent_change_24h
				} else {
					$c = $cryptos|Sort-Object -Property percent_change_24h -Descending
				}
				$c[0..($Limit-1)]|ft name, symbol,rank, price_btc, @{Label="price_gbp"; Expression={"£{0:N2}" -f [double]$_.price_gbp}}, @{Label="24h_volume_gbp"; Expression={"£{0:N2}" -f [double]$_.'24h_volume_gbp'}}, @{Label="market_cap_gbp"; Expression={"£{0:N0}" -f [double]$_.market_cap_gbp}}, *change*
			} elseif ($Change7d) {
				if ($Worst) {
					$c = $cryptos|Sort-Object -Property percent_change_7d
				} else {
					$c = $cryptos|Sort-Object -Property percent_change_7d -Descending
				}
				$c[0..($Limit-1)]|ft name, symbol,rank, price_btc, @{Label="price_gbp"; Expression={"£{0:N2}" -f [double]$_.price_gbp}}, @{Label="24h_volume_gbp"; Expression={"£{0:N2}" -f [double]$_.'24h_volume_gbp'}}, @{Label="market_cap_gbp"; Expression={"£{0:N0}" -f [double]$_.market_cap_gbp}}, *change*
			} else {
				if ($Worst) {
					$cryptos[0 .. ($Limit-1)]|Sort-Object -Property rank|ft name, symbol,rank, price_btc, @{Label="price_gbp"; Expression={"£{0:N2}" -f [double]$_.price_gbp}}, @{Label="24h_volume_gbp"; Expression={"£{0:N2}" -f [double]$_.'24h_volume_gbp'}}, @{Label="market_cap_gbp"; Expression={"£{0:N0}" -f [double]$_.market_cap_gbp}}, *change*
				} else {
					$cryptos[0 .. ($Limit-1)]|ft name, symbol,rank, price_btc, @{Label="price_gbp"; Expression={"£{0:N2}" -f [double]$_.price_gbp}}, @{Label="24h_volume_gbp"; Expression={"£{0:N2}" -f [double]$_.'24h_volume_gbp'}}, @{Label="market_cap_gbp"; Expression={"£{0:N0}" -f [double]$_.market_cap_gbp}}, *change*
				}
			}
		} else {
			if ($Worst) {
				$cryptos|Sort-Object -Property rank -Descending|ft name,symbol,rank, price_btc,*gbp,*change*
			} else {
				$cryptos|ft name, symbol,rank, price_btc, @{Label="price_gbp"; Expression={"£{0:N2}" -f [double]$_.price_gbp}}, @{Label="24h_volume_gbp"; Expression={"£{0:N2}" -f [double]$_.'24h_volume_gbp'}}, @{Label="market_cap_gbp"; Expression={"£{0:N0}" -f [double]$_.market_cap_gbp}}, *change*
			}
			
		}
	}
}